import * as Yup from 'yup';
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

export const valid =
    Yup.string()
        .min(8, 'Must be at least 8 characters')
        .max(20, 'Must be less  than 20 characters')
        .required('Field is required')
        .matches(
            /^[a-zA-Z0-9]+$/,
            'Cannot contain special characters or spaces'
        )
export const valid_email =
    Yup.string()
        .email('Must be a valid email')
        .required('Field is required')
        
export const valid_phone =
    Yup.string()
    .matches(phoneRegExp, 'Phone number is not valid')
