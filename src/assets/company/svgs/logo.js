
export  function Logo(){
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="119" height="85" viewBox="0 0 119 85">

    <g id="Group_179" data-name="Group 179" transform="translate(-307.338 -218.688)">
      <text id="silicon" transform="translate(419.338 260.689)" fill="#fff" fontSize="48" ><tspan x="-111.424" y="0">silicon</tspan></text>
      <text id="Arena_" data-name="Arena&gt;" transform="translate(426.338 287.689)" fill="#fff" fontSize="48" ><tspan x="-117.568" y="0">Arena&gt;</tspan></text>
    </g>
  </svg>
  
  )
}

export  function LogoDark(){
  return (
<svg xmlns="http://www.w3.org/2000/svg" width="119" height="85" viewBox="0 0 119 85">
  <g id="Group_179" data-name="Group 179" transform="translate(-307.338 -218.688)">
    <text id="silicon" transform="translate(419.338 260.689)" fill="#13144e" fontSize="48" ><tspan x="-111.424" y="0">silicon</tspan></text>
    <text id="Arena_" data-name="Arena&gt;" transform="translate(426.338 287.689)" fill="#13144e" fontSize="48" ><tspan x="-117.568" y="0">Arena&gt;</tspan></text>
  </g>
</svg>
  
  )
}