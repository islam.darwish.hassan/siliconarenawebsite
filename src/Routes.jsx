/**
 * External Dependencies
 */
import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

/**
 * Internal Dependencies
 */
import RoutesList from './pages/index';
import RouteWrapper from './RouteWrapper';

/**
 * Component
 */
class Routes extends Component {
     capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
      }
      
    render() {
        const {
            location,
        } = this.props;

        return (
            <Switch
                location={location}
            >
                {Object.keys(RoutesList).map((path) => {
                    const RouteInner = RoutesList[path];
                    return (
                        <Route
                            key={path}
                            path={path}
                            exact
                            render={() => (
                                <RouteWrapper title={"SiliconArena - "+ this.capitalizeFirstLetter(path.substring(1))}
                                ><RouteInner {...this.props} /></RouteWrapper>
                            )}
                        />
                    );
                })}

                { /* 404 */}
                <Route
                    render={() => (

                        //  <NotFoundPage />
                        null
                    )}
                />
            </Switch>
        );
    }
}

export default Routes;
