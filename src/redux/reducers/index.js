/**
 * External Dependencies
 */
import { combineReducers } from "redux";
/**
 * Reducer
 */
import settings from "./settings";
import toast from "./toast";

const rootReducer = combineReducers({
  settings,
  toast
});

export default rootReducer;
