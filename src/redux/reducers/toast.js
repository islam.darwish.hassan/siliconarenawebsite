import { ADD_TOAST, REMOVE_TOAST } from "../types";
import { getUID } from "../../utils";
import Omit from "object.omit";
 
const INITIAL_TOASTS_STATE = [];
export default function toast(state = INITIAL_TOASTS_STATE, action) {
  switch (action.type) {
    case ADD_TOAST:
      const newData = {
        ...{
          title: "",
          content: "",
          color: "brand",
          time: false,
          duration: 0,
          closeButton: true,
        },
        ...action.data,
      };

      if (newData.time === true) {
        newData.time = new Date();
      }

      return {
        ...state,
        [getUID()]: newData,
      };
    case REMOVE_TOAST:
      if (!action.id || !state[action.id]) {
        return state;
      }
      return Omit(state, action.id);
    default:
      return state;
  }
}
