import { UPDATE_SETTINGS } from "../types";
import defaultSettings from '../../settings';
const INITIAL_SETTINGS_STATE = {
    ...defaultSettings,
};

export default function settings ( state = INITIAL_SETTINGS_STATE, action ){
    switch ( action.type ) {
    case UPDATE_SETTINGS:
        return Object.assign( {}, state, action.settings );
    default:
        return state;
    }
}