
import {ADD_TOAST,REMOVE_TOAST} from  '../types'

export const addToast = ( data ) => ( {
    type: ADD_TOAST,
    data,
} );

export const removeToast = ( id ) => ( {
    type: REMOVE_TOAST,
    id,
} );
