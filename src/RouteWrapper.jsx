import React from "react"

class RouteWrapper extends React.Component {

  componentDidMount() {
    document.title = this.props.title
  }
  

  render() {

    return (
        this.props.children
    )
  }
}

export default RouteWrapper
