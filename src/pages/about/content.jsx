
export function P1() {
    return (
        <p className="p-main"><a href="#" className="link-text">SiliconArena</a> doesn’t only stop at developing concrete business solutions , it also offers each client the luxury of getting uniquely attractive designed software solutions to help the client interpret who they really are among their peers.
        </p>
    )
}

export function P2() {
    return (
        <p className="p-main ">
            Users will be engaged and digital transformation will be sparked with custom enterprise software development services.
        </p>)
}

