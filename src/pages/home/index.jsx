import React, { Component } from 'react'
import { Col,  } from 'react-bootstrap';
import PageWrapper from "../../components/_wrappers/page";
import SectionMainWrapper from "../../components/_wrappers/section_main";
import art1 from "../../assets/art/svgs/art1.svg";

import { P1, P2, Cards, P3, P4 } from "./content";
import HeaderWrapper from "../../components/_wrappers/headers/main";
import CardGridWrapper from "../../components/_wrappers/cards_grid";
import CardWrapper from "../../components/_wrappers/cards/main";
import CardImageWrapper from "../../components/_wrappers/cards/image";
import CardHeaderWrapper from "../../components/_wrappers/cards/header";
import {WorkBtn} from "../../components/btns";
import FormWrapper from "../../components/_wrappers/form";
import {WorkWithUs} from "../../components/forms";

export default class index extends Component {

    render() {
        return (
            <PageWrapper >
                <SectionMainWrapper>
                    <Col lg="6" md="12"><img className="img-cover" src={art1} /></Col>
                    <Col lg="6" md="12">
                        <P1 />
                        <P2 />
                    </Col>
                    <WorkBtn/>

                </SectionMainWrapper>
                <HeaderWrapper>
                    <div className="text-header">
                        <h4 className="text-uppercase">Our services</h4>
                        <h1 className="text-big text-uppercase">What We Do</h1>
                    </div>
                </HeaderWrapper>
                <CardGridWrapper>
                    <Cards />
                </CardGridWrapper>
                <HeaderWrapper>
                    <div className="text-header">
                        <h4 className="text-uppercase">Success Partners</h4>
                        <h1 className="text-big text-uppercase">Clients</h1>
                    </div>
                </HeaderWrapper>

                <HeaderWrapper>
                    <div className="text-header">
                        <h4 className="text-uppercase">Let's work together</h4>
                        <h1 className="text-big ">Keep in touch</h1>
                    </div>
                </HeaderWrapper>
                <SectionMainWrapper className="align-items-center">
                    <Col lg="8" md="12">
                        <CardWrapper  big nocol>
                            <CardHeaderWrapper>
                                <a href="#">{"Ready to Take Your Ideas to the Next Level?"}</a>
                            </CardHeaderWrapper>
                            <CardImageWrapper >
                                <img className="img-cover"  src={art1} />
                            </CardImageWrapper>
                        </CardWrapper>
                    </Col>
                    <Col lg="4" md="12">
                        <P3 />
                        <P4 />
                    </Col>
                </SectionMainWrapper>
                <FormWrapper>
                    <WorkWithUs/>
                </FormWrapper>
            </PageWrapper>
        )
    }
}
