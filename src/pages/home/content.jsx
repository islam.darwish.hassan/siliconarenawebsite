import art2 from "../../assets/art/svgs/art2.svg";
import art3 from "../../assets/art/svgs/art3.svg";
import art4 from "../../assets/art/svgs/art4.svg";
import art5 from "../../assets/art/svgs/art5.svg";
import art6 from "../../assets/art/svgs/art6.svg";
import art7 from "../../assets/art/svgs/art7.svg";

import CardWrapper from "../../components/_wrappers/cards/main";
import CardImageWrapper from "../../components/_wrappers/cards/image";
import CardHeaderWrapper from "../../components/_wrappers/cards/header";

export function P1() {
    return (
        <>
        <p className="p-main mt-2"><a href="#" className="link-text">SiliconArena</a> is a software development house where we transform our partners ideas into a full featured digital products.</p>
        </>
    )
}

export function P2() {
    return (
        <>
        <p className="p-main ">
            We design and develop professional business solutions which are presented to our customers to cover the business needs in excellent quality and reasonable cost.

        </p>
        </>
        )
}
export function P3() {
    return (
        <p className="p-main">Tell us a little bit about your app development needs by completing the form below, and an expert will reach out within 24-48 hours to schedule your 
        consultation.</p>
    )
}

export function P4() {
    return (
        <p className="p-main ">
           Custom software can offer a unique competitive advantage or improve customer experience exponentially in a way that commercial off-the-shelf software cannot.

        </p>)
}
export function Cards() {
    return (
        <>
            <CardWrapper >
                <CardHeaderWrapper>
                    {"<Game Development>"}
                </CardHeaderWrapper>
                <CardImageWrapper>
                    <img className="img-cover" src={art2} />
                </CardImageWrapper>
            </CardWrapper>
            <CardWrapper >
                <CardHeaderWrapper>
                    {"<Mobile App Development>"}
                </CardHeaderWrapper>
                <CardImageWrapper>
                    <img className="img-cover" src={art3} />
                </CardImageWrapper>
            </CardWrapper>
            <CardWrapper >
                <CardHeaderWrapper>
                    {"<Web Development>"}
                </CardHeaderWrapper>
                <CardImageWrapper>
                    <img className="img-cover" src={art4} />
                </CardImageWrapper>
            </CardWrapper>
            <CardWrapper >
                <CardHeaderWrapper>
                    {"<UI/UX Design>"}
                </CardHeaderWrapper>
                <CardImageWrapper>
                    <img className="img-cover" src={art5} />
                </CardImageWrapper>
            </CardWrapper>

            <CardWrapper >
                <CardHeaderWrapper>
                    {"<VR - AR>"}
                </CardHeaderWrapper>
                <CardImageWrapper>
                    <img className="img-cover" src={art6} />
                </CardImageWrapper>
            </CardWrapper>

            <CardWrapper >
                <CardHeaderWrapper>
                    {"<Consultations>"}
                </CardHeaderWrapper>
                <CardImageWrapper>
                    <img className="img-cover" src={art7} />
                </CardImageWrapper>
            </CardWrapper>


        </>
    )
}