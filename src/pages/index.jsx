/**
 * Internal Dependencies
 */

import Home from './home'
import Contact from './contact'
import About from './about'
import Blog from './blog'
import Careers from './careers'
import Services from './services'

//Previews

import Arts from "./_previews/art";

export default {
    '/': Home,
    '/contact':Contact,
    '/about':About,
    '/blog':Blog,
    '/careers':Careers,
    '/services':Services,
    



    //previews
    '/art':Arts
};
