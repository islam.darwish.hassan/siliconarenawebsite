export function P1() {
  return (
    <p className="p-main">
      <a href="#" className="link-text">
        SiliconArena
      </a>{' '}
      is operated by talented programmers and creative designers with the
      objective of fostering a good work environment.
    </p>
  )
}

export function P2() {
  return (
    <p className="p-main ">
      You'll acquire expertise creating things from the ground up and delivering
      shippable solutions for a range of technologies at SiliconArena.
    </p>
  )
}
