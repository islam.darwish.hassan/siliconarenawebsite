import React, { Component } from 'react'
import { Col,  } from 'react-bootstrap';
import PageWrapper from "../../components/_wrappers/page";
import SectionMainWrapper from "../../components/_wrappers/section_main";
import art20 from "../../assets/art/svgs/art20.svg";

import { P1, P2 } from "./content";
export default class index extends Component {
    render() {
        return (
            <PageWrapper >
                <SectionMainWrapper>
                    <Col lg="6" md="12">
                        <P1 />
                        <P2 />
                    </Col>
                    <Col lg="6" md="12"><img className="img-cover" src={art20} alt=""/></Col>

                </SectionMainWrapper>

            </PageWrapper>
        )
    }
}
