
export function P1() {
    return (
        <p className="p-main">NOW IS THE TIME TO Launch YOUR <a href="#">NEXT BIG IDEA </a> OFF THE GROUND 🚀</p>
    )
}

export function P2() {
    return (
        <p className="p-main ">
            Users will be engaged and digital transformation will be sparked with custom enterprise software development services.
        </p>)
}

export function P3() {
    return (
        <>
            <a href="#" className="p-main">Web Development</a>
            <p className="p-main ">
                SiliconArena helps it’s clients to build their online professional identities for their brands, using high performing technologies to produce custom web development for businesses to provide flexible, reliable, and engaging solutions for their users. It all starts with our team of UI/UX designers and developers who help define requirements, hence our expert team of developers who have excelled in PHP and React to implement the ideas.
            </p>
        </>
    )

}

export function P4() {
    return (
        <>
            <a href="#" className="p-main">Mobile Development</a>
            <p className="p-main ">
            Get the most out of your application by developing a cross-platform app! Cross-platform development is the best approach to cut the Project cost of developing a mobile application. It assists the businesses to diminish upgraded charges time-to-market and obtain more users and followers without scarifying the quality.
            </p>
        </>
    )

}

export function P5() {
    return (
        <>
            <a href="#" className="p-main">Game Development</a>
            <p className="p-main ">
            We build games with a vision of blending fantasy and realism and we are dedicated to creating games and immersive worlds that bring players closer together. 🎮🎯            </p>
        </>
    )

}

export function P6() {
    return (
        <>
            <a href="#" className="p-main">VR -AR - MR </a>
            <p className="p-main ">
            The ongoing virtual reality, augmented reality, and mixed-reality revolution demonstrates the power and potential of immersion in specialty systems and use cases. Challenges such as battery life, computer vision, and pixel density beyond 4K must be addressed for successful AR and VR experiences. </p>
        </>
    )

}

export function P7() {
    return (
        <>
            <a href="#" className="p-main">UX-UI Design</a>
            <p className="p-main ">
            No matter what your needs are, our passionate  designers  Certified Designers from Google are ready to design all types of mobile apps and websites in different categories.
          </p>
        </>
    )

}

export function P8() {
    return (
        <>
            <p className="p-main ">
            Because design is all about the contact points that a user meets when navigating from point A to point B, we pay close attention to these small elements in order to offer an engaging experience for your consumers.          
            </p>
        </>
    )

}

export function P9() {
    return (
        <>
             <a href="#" className="p-main">Consultations</a>
            <p className="p-main ">
            Our Technology Strategy and Advisory practice designs and executes industry-relevant reinventions that allow organizations to realize exceptional business value from technology.
            </p>
        </>
    )

}
