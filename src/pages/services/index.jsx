import React, { Component } from 'react'
import { Col,  } from 'react-bootstrap';
import PageWrapper from "../../components/_wrappers/page";
import SectionMainWrapper from "../../components/_wrappers/section_main";
import art14 from "../../assets/art/svgs/art14.svg";
import art15 from "../../assets/art/svgs/art15.svg";
import art3 from "../../assets/art/svgs/art3.svg";
import art2 from "../../assets/art/svgs/art2.svg";
import art5 from "../../assets/art/svgs/art5.svg";
import art6 from "../../assets/art/svgs/art6.svg";
import art7 from "../../assets/art/svgs/art7.svg";

import HeaderWrapper from "../../components/_wrappers/headers/main";

import { P1, P2, P3, P4, P5, P6, P7,P8 ,P9} from "./content";
export default class index extends Component {
    render() {
        return (
            <PageWrapper >
                <SectionMainWrapper>
                    <Col lg="6" md="12">
                        <P1 />
                        <P2 />
                    </Col>
                    <Col lg="6" md="12"><img className="img-cover" src={art14} alt="" /></Col>
                </SectionMainWrapper>
                <HeaderWrapper>
                    <div className="text-header">
                        <h4 className="text-uppercase">Our Services</h4>
                        <h1 className="text-big text-uppercase">What We Do</h1>
                    </div>
                </HeaderWrapper>
                <SectionMainWrapper>
                    <Col lg="6" md="12"><img className="img-cover" src={art15} alt="" /></Col>
                    <Col lg="6" md="12">
                        <P3 />
                    </Col>
                </SectionMainWrapper>
                <SectionMainWrapper>
                    <Col lg="6" md="12">
                        <P4 />
                    </Col>
                    <Col lg="6" md="12"><img className="img-cover" src={art3} alt="" /></Col>

                </SectionMainWrapper>
                <SectionMainWrapper className="align-items-center">
                    <Col lg="6" md="12"><img className="img-cover" src={art2} alt="" /></Col>

                    <Col lg="6" md="12">
                        <P5 />
                    </Col>
                </SectionMainWrapper>
                <SectionMainWrapper className="align-items-center">
                    <Col lg="6" md="12">
                        <P6 />
                    </Col>

                    <Col lg="6" md="12"><img className="img-cover" src={art6} alt="" /></Col>
                </SectionMainWrapper>
                <SectionMainWrapper >
                    <Col lg="6" md="12"><img className="img-cover" src={art5} alt="" /></Col>
                    <Col lg="6" md="12">
                        <P7 />
                        <P8 />
                    </Col>
                </SectionMainWrapper>
                <SectionMainWrapper className="align-items-start" >
                    <Col lg="6" md="12">
                        <P9 />
                    </Col>
                    <Col lg="6" md="12"><img className="img-cover" src={art7} alt="" /></Col>
                </SectionMainWrapper>

            </PageWrapper>
        )
    }
}
