/**
 * External Dependencies
 */
import React, { Component } from "react";
import { withRouter, HashRouter, BrowserRouter } from "react-router-dom";
import { CSSTransition, TransitionGroup } from "react-transition-group";

/**
 * Internal Dependencies
 */
import Routes from "./Routes";



/**
 * Component PageWrap
 */
class PageWrap extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {  location } = this.props;

    return (
      <TransitionGroup>
        <CSSTransition
          key={location.pathname}
          timeout={300}
          classNames="rui-router-transition"
          unmountOnExit
        >
          <Routes location={location} />
        </CSSTransition>
      </TransitionGroup>
    );
  }
}

const PageWrapWithRouter = withRouter(PageWrap);

/**
 * Component App
 */
class App extends Component {

  render() {
    return (
        <HashRouter>
          <PageWrapWithRouter />
        </HashRouter>
    );
  }
}

export default App;
