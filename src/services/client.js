import axios from "axios";

const local = "http://localhost:8000";
const Global = "https://api.silicon-arena.com";

export const client = axios.create({
  baseURL: `${Global}/api`,
  timeout: 20000,
});

client.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.token}`;
  return config;
});
