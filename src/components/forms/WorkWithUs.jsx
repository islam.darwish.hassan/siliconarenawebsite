import React from 'react'
import { Col, Row } from 'react-bootstrap';
import art12 from "../../assets/art/svgs/art12.svg";
import { useFormik, FormikProvider, Form, useField } from 'formik';
import * as Yup from 'yup';
import { TextInputLiveFeedback } from '../_wrappers/form/fields/TextInput';

import { valid, valid_email, valid_phone } from "../../utils/Validator";

export default function WorkWithUs() {
    const formik = useFormik({
        initialValues: {
            name: '', email: '', phone: ''
        },
        onSubmit: async (values) => {
            alert(JSON.stringify(values, null, 2));
        },
        validationSchema: Yup.object({
            name: valid,
            email: valid_email,
            phone: valid_phone

        }),
    });


    return (
        <div>
            <Row>
                <Col lg="6">
                    <FormikProvider value={formik}>
                        <Form >
                            <TextInputLiveFeedback
                                label="Username"
                                id="name"
                                name="name"
                                type="text"
                            />

                            <TextInputLiveFeedback
                                label="E-mail"
                                id="email"
                                name="email"
                                type="email"
                            />

                            <TextInputLiveFeedback
                                label="Phone"
                                id="phone"
                                name="phone"
                                type="text"
                            />
                            <div className="d-flex justify-content-center my-2">
                                <button className="btn btn-secondary  w-100 mx-2" type="submit">Submit</button>
                            </div>
                        </Form>
                    </FormikProvider>
                </Col>
                <Col lg="6">
                    <img className="img-cover" src={art12} alt="" />

                </Col>

            </Row>
        </div>
    )
}
