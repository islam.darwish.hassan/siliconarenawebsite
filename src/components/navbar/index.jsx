import React from 'react'
import { Col, Container, Nav, Navbar, NavDropdown, Row } from 'react-bootstrap'
import { Logo } from "../../assets/company/svgs/logo.js";
import {useLocation} from "react-router-dom";
export default function Index() {

    const location = useLocation()

    return (
        
        <>
            <Navbar collapseOnSelect expand="lg" className="main-navbar" variant="dark">
                <Container>
                    <Navbar.Brand href="#home">
                        <Logo />
                        {' '}
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mx-auto">
                            <Nav.Link href="/" className={location.pathname=="/"?'active':null} >Home</Nav.Link>
                            <Nav.Link href="#/services"  className={location.pathname=="/services"?'active':null} >Services</Nav.Link>
                            <Nav.Link href="#/about"  className={location.pathname=="/about"?'active':null} >About</Nav.Link>
                            <Nav.Link href="#/careers"  className={location.pathname=="/careers"?'/active':null} >Careers</Nav.Link>
                            <Nav.Link href="#/blog"  className={location.pathname=="/blog"?'/active':null} >Blog</Nav.Link>

                        </Nav>
                    </Navbar.Collapse>
                    <Nav.Link href="#/blog" className="lets-btn">Let's Talk 👋 </Nav.Link>

                </Container>
            </Navbar>
        </>)
}
