import React from 'react'
import { Nav } from 'react-bootstrap'

export default function work() {
    return (
        <div className="my-3 d-sm-flex justify-content-end">
            <Nav.Link href="#/blog" className="lets-btn">Let's Work Together  🤝 </Nav.Link>
        </div>

    )
}
