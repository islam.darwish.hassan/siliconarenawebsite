import React from 'react'
import dots from "../../../assets/shapes/svgs/dots.svg"
export default function main(props) {
    return (
        <div className="header-container d-flex justify-content-center align-items-end my-5">
        <img className="shape-dots" src={dots} alt=""/>
            <div className="p-3 ">
            {props.children}
            </div>
        </div>
    )
}
