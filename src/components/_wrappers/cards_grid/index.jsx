import React from 'react'
import { Container, Row } from 'react-bootstrap'

export default function index(props) {
    return (
        <>
            <Container >
                <Row className="pt-5" >
                    {props.children}
                </Row>
            </Container>
        </>
    )
}
