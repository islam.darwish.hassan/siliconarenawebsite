import React from 'react'

export default function header({ children }) {
    return (
        <>
            <div className="d-flex justify-content-center  ">
                <span className="text-card-header">            
                {children}
                </span>
            </div>

        </>
    )
}
