import React from 'react'
import { Col } from 'react-bootstrap'
export default function main(props) {
    return (
        props.nocol?
        <div className={`card ${props.big?'card-big':'card-min'}`}>
            {props.children}
        </div>
        :
        <Col lg="4" md="6" sm="12">
        <div className={`card ${props.big?'card-big':'card-min'}`}>
                {props.children}
            </div>
        </Col>
    )
}
