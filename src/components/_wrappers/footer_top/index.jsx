import React from 'react';
import footer_bg from "../../../assets/shapes/svgs/footer.svg";
import { LogoDark } from "../../../assets/company/svgs/logo.js";
import { Col, Container, Row } from 'react-bootstrap';
import { P1, P2, P3 } from "./content";
import fb from "../../../assets/social/svgs/fb.svg";
import lin from "../../../assets/social/svgs/in.svg";
import insta from "../../../assets/social/svgs/insta.svg";
import yt from "../../../assets/social/svgs/yt.svg";

export default function index({ children }) {
    return (
        <div >
            <div style={{ backgroundImage: `url(${footer_bg})` }} className="shape-footer pt-5 ">
                <Container className="p-lg-5">
                    <LogoDark />
                    <Row className="p-lg-2 mt-5 ">
                        <Col lg="4" md="6" sm="12"><P1 /></Col>
                        <Col lg="4" md="6" sm="12"><P2 /></Col>
                        <Col lg="4" md="6" sm="12"><P3 /></Col>
                    </Row>
                    <div className="social-links d-flex justify-content-center">
                        <div ><img src={fb} alt="" /></div>
                        <div ><img src={lin} alt="" /></div>
                        <div ><img src={insta} alt=""/></div>
                        <div ><img src={yt} alt=""/></div>

                    </div>
                </Container>
            </div>
            <div />
        </div>
    )
}
