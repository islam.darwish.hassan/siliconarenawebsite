export function P1() {
    return (
        <>
        <p className="p-main"><span  className="link-text">SiliconArena</span> is a software development house where we transform our partners ideas into a full featured digital products.</p>
        <a href="https://portfolio.silicon-arena.com" className="fw-bold d-flex mb-3 justify-content-center btn btn-outline-secondary">Preview Company Profile</a>
        </>
    )
}

export function P2() {
    return (
        <>
            <h5 className="fw-bold">INFORMATION</h5>
            <ul className="p-main ">
                <li>SiliconArena Blog
                </li>
                <li>SiliconArena Careers
                </li>
                <li>Find us on Clutch
                </li>
                <li>Find us on Glassdoor
                </li>

            </ul>
        </>
    )
}

export function P3() {
    return (
        <div>
            <h5 className="fw-bold">Contact Us</h5>
            <ul className="p-main">
                <li>(+20) 12-77-444-078
                </li>
                <li>Hello@silicon-Arena.com
                </li>
                <li>171 The Greek Campus , Eltahrir , Cairo , Egypt.
                </li>

            </ul>
        </div>
    )
}
