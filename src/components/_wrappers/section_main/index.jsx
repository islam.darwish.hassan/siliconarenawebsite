import React from 'react'
import { Container, Nav, Row } from 'react-bootstrap'

export default function index(props) {
    return (
        <>
            <Container>
                <Row className={`p-5 ${props.className ? props.className : 'align-items-end '}`} >
                    {props.children}

                </Row>
            </Container>
        </>
    )
}
