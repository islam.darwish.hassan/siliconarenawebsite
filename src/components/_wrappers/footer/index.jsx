import React from 'react'
import heart from "../../../assets/shapes/svgs/heart.svg";
export default function index({ children }) {
    return (
        <footer className="d-flex justify-content-center p-2 ">
            Copyright © 2021 - Made with <img src={heart} alt="" className="px-2" /> by SiliconArena. All rights reserved.
            {children}
        </footer>
    )
}
