import React from 'react'

export default function main(props) {
    return (
        <>
            {props.children}
        </>
    )
}
