import React from 'react'
import curves from "../../../assets/shapes/svgs/curves.svg";
import FooterWrapper from "../../../components/_wrappers/footer";
import FooterTopWrapper from "../../../components/_wrappers/footer_top";
import Navbar from "../../navbar";
export default function index(props) {
    return (
        <div>
             <img src={curves} className="shape-curves" alt=""/>
             <Navbar/>
            {props.children}
            <FooterTopWrapper />
            <FooterWrapper/>

        </div>
    )
}
